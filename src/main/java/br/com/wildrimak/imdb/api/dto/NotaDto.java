package br.com.wildrimak.imdb.api.dto;

public class NotaDto {

    private Integer nota;

    public Integer getNota() {
	return nota;
    }

    public void setNota(Integer nota) {
	this.nota = nota;
    }

}
