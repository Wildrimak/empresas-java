package br.com.wildrimak.imdb.api.requests;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import br.com.wildrimak.imdb.domain.models.Usuario;

public class UsuarioRequest {

    @Email
    @NotEmpty
    private String email;

    @NotEmpty
    private String password;

    @NotEmpty
    private String nome;

    public UsuarioRequest(@Email @NotEmpty String email, @NotEmpty String password, @NotEmpty String nome) {
	this.email = email;
	this.password = password;
	this.nome = nome;
    }

    public Usuario toModel() {
	return new Usuario(email, password, nome);
    }

}
