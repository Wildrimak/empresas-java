package br.com.wildrimak.imdb.api.requests;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.wildrimak.imdb.domain.models.Filme;

public class FilmeRequest {

    @NotEmpty
    private String nome;

    @NotEmpty
    private String genero;

    @NotEmpty
    private String atores;

    @NotEmpty
    private String diretor;

    @NotEmpty
    private String ano;

    @NotNull
    private Long duracao;

    @NotEmpty
    private String enredo;

    public FilmeRequest(@NotEmpty String nome, @NotEmpty String genero, @NotEmpty String atores,
	    @NotEmpty String diretor, @NotEmpty String ano, @NotNull Long duracao, @NotEmpty String enredo) {
	this.nome = nome;
	this.genero = genero;
	this.atores = atores;
	this.diretor = diretor;
	this.ano = ano;
	this.duracao = duracao;
	this.enredo = enredo;
    }

    public Filme toModel() {
	return new Filme(nome, genero, atores, diretor, ano, duracao, enredo);
    }

}
