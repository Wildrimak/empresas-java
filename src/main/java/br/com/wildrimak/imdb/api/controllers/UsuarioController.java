package br.com.wildrimak.imdb.api.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.wildrimak.imdb.api.requests.UsuarioRequest;
import br.com.wildrimak.imdb.domain.models.Role;
import br.com.wildrimak.imdb.domain.models.Usuario;
import br.com.wildrimak.imdb.domain.repositories.UsuarioRepository;
import br.com.wildrimak.imdb.domain.services.UsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping
	public List<Usuario> getUsers(@RequestParam("pagina") @Nullable Integer pagina) {

		Integer paginaAtual = (pagina == null) ? 0 : pagina;
		int tamanhoPagina = 2;
		Sort sort = Sort.by(Sort.Direction.ASC, "nome");

		return usuarioRepository.findByRoleAndIsAtivoTrue(Role.NORMAL,
				PageRequest.of(paginaAtual, tamanhoPagina, sort));
	}

	@PostMapping
	public ResponseEntity<Usuario> saveUsuario(@Valid @RequestBody UsuarioRequest usuarioRequest,
			UriComponentsBuilder uriComponentsBuilder) {

		Usuario usuario = usuarioRequest.toModel();
		Usuario save = usuarioService.cadastrar(usuario);

		return ResponseEntity.created(uriComponentsBuilder.path("/usuarios/{id}").buildAndExpand(save.getId()).toUri())
				.body(save);

	}

	@PutMapping("/{idUsuario}")
	public ResponseEntity<Usuario> atualizarUsuario(@PathVariable Long idUsuario,
			@Valid @RequestBody UsuarioRequest usuarioRequest) {

		Optional<Usuario> optional = usuarioRepository.findById(idUsuario);

		if (optional.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Usuario usuario = usuarioRequest.toModel();
		Usuario save = usuarioService.atualizar(usuario, idUsuario);

		return ResponseEntity.status(HttpStatus.OK).body(save);

	}

	@DeleteMapping("/{idUsuario}")
	public ResponseEntity<?> deletarUsuario(@PathVariable Long idUsuario) {

		Optional<Usuario> optional = usuarioRepository.findById(idUsuario);

		if (optional.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		usuarioService.deletar(optional.get());

		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

	}

}
