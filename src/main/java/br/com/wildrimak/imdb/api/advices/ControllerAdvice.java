package br.com.wildrimak.imdb.api.advices;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {

	@ExceptionHandler
	public ResponseEntity<?> trataIllegalIllegalAccessError(IllegalAccessError error) {

		String detalhes = error.getMessage();

		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(detalhes);

	}

	@ExceptionHandler
	public ResponseEntity<?> trataIllegalArgumentException(IllegalArgumentException exception) {

		String detalhes = exception.getMessage();

		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(detalhes);

	}

}
