package br.com.wildrimak.imdb.api.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.wildrimak.imdb.api.dto.NotaDto;
import br.com.wildrimak.imdb.api.requests.FilmeRequest;
import br.com.wildrimak.imdb.api.responses.DetalheFilmeResponse;
import br.com.wildrimak.imdb.domain.models.Avaliacao;
import br.com.wildrimak.imdb.domain.models.Filme;
import br.com.wildrimak.imdb.domain.repositories.FilmeRepository;
import br.com.wildrimak.imdb.domain.services.FilmeService;

@RestController
@RequestMapping("/filmes")
public class FilmeController {

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private FilmeService filmeService;

    @PostMapping
    @RequestMapping("/{idFilme}/avaliar")
    public ResponseEntity<Avaliacao> avaliar(@RequestBody NotaDto notaDto, @PathVariable Long idFilme) {

	Optional<Filme> optional = filmeRepository.findById(idFilme);

	if (optional.isEmpty()) {
	    return ResponseEntity.
			status(HttpStatus.NOT_FOUND).build();
	}

	Avaliacao avaliacao = filmeService.avaliar(optional.get(), notaDto.getNota());

	return ResponseEntity.
		status(HttpStatus.OK).body(avaliacao);
	
    }

    @PostMapping
    public ResponseEntity<Filme> saveFilme(@Valid @RequestBody FilmeRequest filmeRequest, UriComponentsBuilder uriComponentsBuilder) {

	Filme filme = filmeRequest.toModel();
	Filme save = filmeService.salvar(filme);

	return ResponseEntity.created(uriComponentsBuilder.path("/filmes/{id}").buildAndExpand(save.getId()).toUri())
		.body(save);
    }

    @GetMapping
    public List<DetalheFilmeResponse> getFilmes(@RequestParam("pagina") @Nullable Integer pagina) {

	List<DetalheFilmeResponse> filmesResponse = filmeService.getFilmes(pagina);
	return filmesResponse;

    }

    @GetMapping("/{idFilme}")
    public Filme detalheFilme(@PathVariable Long idFilme) {

	Optional<Filme> optional = filmeRepository.findById(idFilme);

	if (optional.isEmpty()) {
	    return null;
	}

	Filme filme = optional.get();

	return filme;

    }

}
