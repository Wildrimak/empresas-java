package br.com.wildrimak.imdb.api.responses;

import br.com.wildrimak.imdb.domain.models.Filme;

public class DetalheFilmeResponse {

    private Long id;

    private String nome;

    private String genero;

    private String atores;

    private String diretor;

    private String ano;

    private Long duracao;

    private String enredo;

    private Long votosTotais;

    public DetalheFilmeResponse(Filme filme, Long votos) {

	this.id = filme.getId();
	this.nome = filme.getNome();
	this.genero = filme.getGenero();
	this.atores = filme.getAtores();
	this.diretor = filme.getDiretor();
	this.ano = filme.getAno();
	this.duracao = filme.getDuracao();
	this.enredo = filme.getEnredo();
	this.votosTotais = votos;

    }

    public Long getId() {
	return id;
    }

    public String getNome() {
	return nome;
    }

    public String getGenero() {
	return genero;
    }

    public String getAtores() {
	return atores;
    }

    public String getDiretor() {
	return diretor;
    }

    public String getAno() {
	return ano;
    }

    public Long getDuracao() {
	return duracao;
    }

    public String getEnredo() {
	return enredo;
    }

    public Long getVotosTotais() {
	return votosTotais;
    }
    
    public void setVotosTotais(Long votosTotais) {
	this.votosTotais = votosTotais;
    }

}
