package br.com.wildrimak.imdb.domain.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.wildrimak.imdb.domain.models.Role;
import br.com.wildrimak.imdb.domain.models.Usuario;
import br.com.wildrimak.imdb.domain.repositories.UsuarioRepository;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Usuario cadastrar(Usuario usuario) {

	if (usuario.getEmail().endsWith("@admin.com")) {
	    usuario.setRole(Role.ADMIN);
	}
	
	String emailEnviado = usuario.getEmail();

	Optional<Usuario> optional = usuarioRepository.findByEmail(emailEnviado);
	
	if (optional.isPresent()) {
	    throw new IllegalArgumentException("Esse email já foi cadastrado...");
	}
	
	usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));
	Usuario save = usuarioRepository.save(usuario);

	return save;

    }

    public Usuario atualizar(Usuario usuarioAtualizado, Long idUsuario) {

	Usuario usuarioAutenticado = getUserFromToken();

	if (usuarioAutenticado.getId() != idUsuario) {
	    throw new IllegalAccessError("Você não pode atualizar dados de outro usuario..");
	}

	Optional<Usuario> optional = usuarioRepository.findById(idUsuario);

	Usuario existente = optional.get();

	usuarioAtualizado.setId(existente.getId());

	return usuarioRepository.save(usuarioAtualizado);
    }

    private Usuario getUserFromToken() {

	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

	String email = (String) authentication.getPrincipal();

	Optional<Usuario> optional = usuarioRepository.findByEmail(email);

	if (optional.isEmpty()) {
	    throw new IllegalStateException("O seu usuario não existe mais...");
	}
	return optional.get();
    }

    public void deletar(Usuario usuarioDesativar) {

	Usuario usuarioAutenticado = getUserFromToken();

	if (usuarioAutenticado.getId() != usuarioDesativar.getId()) {
	    throw new IllegalAccessError("Você não pode desativar a conta de outro usuario...");
	}

	usuarioDesativar.setIsAtivo(false);
	usuarioRepository.save(usuarioDesativar);

    }

}
