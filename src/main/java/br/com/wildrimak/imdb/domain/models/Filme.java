package br.com.wildrimak.imdb.domain.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "filme")
public class Filme {

    @Id
    @Column(name = "id_filme")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    private String nome;

    @NotEmpty
    private String genero;

    @NotEmpty
    private String atores;

    @NotEmpty
    private String diretor;

    @NotEmpty
    @Size(min = 4, max = 4)
    private String ano;

    @NotNull
    private Long duracao;

    @NotEmpty
    private String enredo;

    @ManyToOne
    @JoinColumn(name = "fk_usuario")
    private Usuario usuarioCriador;

    @OneToMany(mappedBy = "filme", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Avaliacao> avaliacoes = new ArrayList<>();

    private Filme() {
    }

    public Filme(@NotEmpty String nome, @NotEmpty String genero, @NotEmpty String atores, @NotEmpty String diretor,
	    @NotEmpty @Size(min = 4, max = 4) String ano, @NotNull Long duracao, @NotEmpty String enredo) {
	this();
	this.nome = nome;
	this.genero = genero;
	this.atores = atores;
	this.diretor = diretor;
	this.ano = ano;
	this.duracao = duracao;
	this.enredo = enredo;
    }

    public Long getId() {
	return id;
    }

    public String getNome() {
	return nome;
    }

    public String getGenero() {
	return genero;
    }

    public String getAtores() {
	return atores;
    }

    public String getDiretor() {
	return diretor;
    }

    public String getAno() {
	return ano;
    }

    public Long getDuracao() {
	return duracao;
    }

    public String getEnredo() {
	return enredo;
    }

    public Usuario getUsuarioCriador() {
	return usuarioCriador;
    }

    public void setUsuarioCriador(Usuario usuarioCriador) {
	this.usuarioCriador = usuarioCriador;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Filme other = (Filme) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}
