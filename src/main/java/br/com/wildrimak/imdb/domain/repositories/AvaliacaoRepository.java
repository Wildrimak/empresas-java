package br.com.wildrimak.imdb.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.wildrimak.imdb.domain.models.Avaliacao;

public interface AvaliacaoRepository extends JpaRepository<Avaliacao, Long> {

}
