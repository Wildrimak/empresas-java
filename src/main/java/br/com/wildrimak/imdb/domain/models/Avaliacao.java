package br.com.wildrimak.imdb.domain.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "avaliacao")
public class Avaliacao {

    @Id
    @Column(name = "id_avaliacao")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "fk_usuario")
    private Usuario usuario;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "fk_filme")
    private Filme filme;

    @NotNull
    private Integer voto;

    private Avaliacao() {

    }

    public Avaliacao(Usuario usuario, Filme filme, @NotNull Integer voto) {
	this();
	this.usuario = usuario;
	this.filme = filme;
	this.voto = voto;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Usuario getUsuario() {
	return usuario;
    }

    public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
    }

    public Filme getFilme() {
	return filme;
    }

    public void setFilme(Filme filme) {
	this.filme = filme;
    }

    public Integer getVoto() {
	return voto;
    }

    public void setVoto(Integer voto) {
	this.voto = voto;
    }

}
