package br.com.wildrimak.imdb.domain.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.wildrimak.imdb.domain.models.Role;
import br.com.wildrimak.imdb.domain.models.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{

    Optional<Usuario> findByEmail(String email);
    
    List<Usuario> findByRoleAndIsAtivoTrue(Role normal, Pageable pageable);

}
