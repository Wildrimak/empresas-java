package br.com.wildrimak.imdb.domain.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.wildrimak.imdb.api.responses.DetalheFilmeResponse;
import br.com.wildrimak.imdb.domain.models.Filme;

public interface FilmeRepository extends JpaRepository<Filme, Long> {

    @Query("SELECT NEW br.com.wildrimak.imdb.api.responses.DetalheFilmeResponse(f, SUM(a.voto) as votos) "
	    + "FROM avaliacao a RIGHT JOIN a.filme f GROUP BY f.id ORDER BY votos DESC, f.nome ASC")
    public List<DetalheFilmeResponse> filmesOrdenadosPorVotosDescENomeAsc(Pageable pageable);

}
