package br.com.wildrimak.imdb.domain.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.wildrimak.imdb.api.responses.DetalheFilmeResponse;
import br.com.wildrimak.imdb.domain.models.Avaliacao;
import br.com.wildrimak.imdb.domain.models.Filme;
import br.com.wildrimak.imdb.domain.models.Role;
import br.com.wildrimak.imdb.domain.models.Usuario;
import br.com.wildrimak.imdb.domain.repositories.AvaliacaoRepository;
import br.com.wildrimak.imdb.domain.repositories.FilmeRepository;
import br.com.wildrimak.imdb.domain.repositories.UsuarioRepository;

@Service
public class FilmeService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private FilmeRepository filmeRepository;

	@Autowired
	private AvaliacaoRepository avaliacaoRepository;

	public Avaliacao avaliar(Filme filme, Integer nota) {

		Usuario usuario = getUserFromToken();
		
		if (usuario.getRole() == Role.ADMIN) {
			throw new IllegalAccessError("Somente usuário comum pode votar");
		}

		Avaliacao avaliacao = new Avaliacao(usuario, filme, nota);

		return avaliacaoRepository.save(avaliacao);
	}

	public Filme salvar(Filme filme) {

		Usuario usuario = getUserFromToken();

		filme.setUsuarioCriador(usuario);

		return filmeRepository.save(filme);
	}

	public List<DetalheFilmeResponse> getFilmes(Integer pagina) {

		Integer paginaAtual = (pagina == null) ? 0 : pagina;
		int tamanhoPagina = 2;

		Pageable page = PageRequest.of(paginaAtual, tamanhoPagina);

		List<DetalheFilmeResponse> filmesResponse = filmeRepository.filmesOrdenadosPorVotosDescENomeAsc(page);

		return filmesResponse;
	}

	private Usuario getUserFromToken() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		String email = (String) authentication.getPrincipal();

		Optional<Usuario> optional = usuarioRepository.findByEmail(email);

		if (optional.isEmpty()) {
			throw new IllegalStateException("O seu usuario não existe mais...");
		}
		return optional.get();
	}
}
