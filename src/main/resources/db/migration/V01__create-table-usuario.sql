create table if not exists usuario(
	id_usuario int auto_increment primary key,
	nome varchar(255) not null,
	email varchar(255) unique not null,
	password varchar(255) not null,
	papel varchar(16) not null,
	is_ativo bool not null
);
