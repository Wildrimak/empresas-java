create table if not exists filme (
	id_filme int auto_increment primary key,
	nome varchar(255) not null,
	genero varchar(255) not null,
	atores varchar(1023) not null,
	diretor varchar(255) not null,
	ano varchar(4) not null,
	duracao long not null,
	enredo varchar(2047) not null,
	fk_usuario int not null,
	foreign key (fk_usuario) references usuario (id_usuario) 
	on update restrict on delete cascade 
);
