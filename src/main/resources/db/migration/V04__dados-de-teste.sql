INSERT INTO imdb.usuario
(id_usuario, nome, email, password, papel, is_ativo)
VALUES(4, 'wildrimak', 'finalwildrimak@admin.com', '$2a$10$SOR8GGy0ev9lJaPqDhQn8eIzcnVQkQjrJ58PnZoP14r6XMHZCb3Mq', 'ADMIN', 1);
INSERT INTO imdb.usuario
(id_usuario, nome, email, password, papel, is_ativo)
VALUES(5, 'wildrimak', 'pafinalwildrimak@admin.com', '$2a$10$wEOTSocGuzP9YATJLj01l.FOvr3RTd1cC/3j9jF2CvSgZZ6IpjiUW', 'ADMIN', 1);
INSERT INTO imdb.usuario
(id_usuario, nome, email, password, papel, is_ativo)
VALUES(6, 'wildrimak', 'rafinalwildrimak@admin.com', '$2a$10$Sv8hawLTIp9Y8cBwmJBPI.fjVDvnottfy1UN9ELaCQGbEegIrGzpu', 'ADMIN', 1);
INSERT INTO imdb.usuario
(id_usuario, nome, email, password, papel, is_ativo)
VALUES(1, 'wildrimak', 'kafinalwildrimak@gmail.com', '$2a$10$jNOEs5TnxFY/o8JxCcC9KOyRFLDeKUzO2U8in9fgxy1B612JTtUfK', 'NORMAL', 1);
INSERT INTO imdb.usuario
(id_usuario, nome, email, password, papel, is_ativo)
VALUES(2, 'wildrimak', 'lafinalwildrimak@gmail.com', '$2a$10$RYOytZvbZQdXOsXHm5rdw.JfntjZczemw4kMSxG7dkuOxPxFH3yDm', 'NORMAL', 1);
INSERT INTO imdb.usuario
(id_usuario, nome, email, password, papel, is_ativo)
VALUES(3, 'wildrimak', 'finalwildrimak@gmail.com', '$2a$10$5aIMN2l7NAaKluB/q.IUGuZ5MYu9giIBVl.L0iTihyMyMQZTlYEo2', 'NORMAL', 1);


INSERT INTO imdb.filme
(id_filme, nome, genero, atores, diretor, ano, duracao, enredo, fk_usuario)
VALUES(1, 'el camino', 'qualquer', 'qualquer', 'qualquer', '2020', '20001000', 'qualquer', 6);
INSERT INTO imdb.filme
(id_filme, nome, genero, atores, diretor, ano, duracao, enredo, fk_usuario)
VALUES(2, 'senhor dos aneis', 'qualquer', 'qualquer', 'qualquer', '2020', '20001000', 'qualquer', 6);
INSERT INTO imdb.filme
(id_filme, nome, genero, atores, diretor, ano, duracao, enredo, fk_usuario)
VALUES(3, 'crepusculo', 'qualquer', 'qualquer', 'qualquer', '2020', '20001000', 'qualquer', 6);
INSERT INTO imdb.filme
(id_filme, nome, genero, atores, diretor, ano, duracao, enredo, fk_usuario)
VALUES(4, 'harry potter', 'qualquer', 'qualquer', 'qualquer', '2020', '20001000', 'qualquer', 6);
INSERT INTO imdb.filme
(id_filme, nome, genero, atores, diretor, ano, duracao, enredo, fk_usuario)
VALUES(5, 'smiliguido', 'qualquer', 'qualquer', 'qualquer', '2020', '20001000', 'qualquer', 6);
INSERT INTO imdb.filme
(id_filme, nome, genero, atores, diretor, ano, duracao, enredo, fk_usuario)
VALUES(6, 'turma da festa', 'qualquer', 'qualquer', 'qualquer', '2020', '20001000', 'qualquer', 6);
INSERT INTO imdb.filme
(id_filme, nome, genero, atores, diretor, ano, duracao, enredo, fk_usuario)
VALUES(7, 'ivocacao do mal', 'qualquer', 'qualquer', 'qualquer', '2020', '20001000', 'qualquer', 6);
INSERT INTO imdb.filme
(id_filme, nome, genero, atores, diretor, ano, duracao, enredo, fk_usuario)
VALUES(8, 'berço do amanha', 'qualquer', 'qualquer', 'qualquer', '2020', '20001000', 'qualquer', 6);
INSERT INTO imdb.filme
(id_filme, nome, genero, atores, diretor, ano, duracao, enredo, fk_usuario)
VALUES(9, 'lalaland', 'qualquer', 'qualquer', 'qualquer', '2020', '20001000', 'qualquer', 6);

INSERT INTO imdb.avaliacao
(id_avaliacao, fk_usuario, fk_filme, voto)
VALUES(1, 1, 1, 3);
INSERT INTO imdb.avaliacao
(id_avaliacao, fk_usuario, fk_filme, voto)
VALUES(2, 2, 1, 2);
INSERT INTO imdb.avaliacao
(id_avaliacao, fk_usuario, fk_filme, voto)
VALUES(3, 3, 1, 3);
INSERT INTO imdb.avaliacao
(id_avaliacao, fk_usuario, fk_filme, voto)
VALUES(4, 1, 2, 4);
INSERT INTO imdb.avaliacao
(id_avaliacao, fk_usuario, fk_filme, voto)
VALUES(5, 2, 2, 4);
INSERT INTO imdb.avaliacao
(id_avaliacao, fk_usuario, fk_filme, voto)
VALUES(6, 3, 2, 4);
INSERT INTO imdb.avaliacao
(id_avaliacao, fk_usuario, fk_filme, voto)
VALUES(7, 1, 3, 3);
INSERT INTO imdb.avaliacao
(id_avaliacao, fk_usuario, fk_filme, voto)
VALUES(8, 2, 3, 2);
INSERT INTO imdb.avaliacao
(id_avaliacao, fk_usuario, fk_filme, voto)
VALUES(9, 3, 3, 3);


