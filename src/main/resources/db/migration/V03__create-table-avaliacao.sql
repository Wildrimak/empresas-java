create table if not exists avaliacao (
	id_avaliacao int auto_increment primary key,
	fk_usuario int not null,
	fk_filme int not null,
	voto int not null,
	foreign key (fk_usuario) references usuario (id_usuario) 
	on update restrict on delete cascade,
	foreign key (fk_filme) references filme (id_filme) 
	on update restrict on delete cascade
);